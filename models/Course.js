const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : false
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	enrollees : [
	{
		userId : {
			type : String,
			required : [true, "UserId is required"]
		},
		enrolledOn : {
			type : Date,
			default : new Date()
		}
	}
	]
});


// "module.exports" is a way for Node JS to treat the value as a package that can be used by other files
module.exports = mongoose.model("Course", courseSchema);