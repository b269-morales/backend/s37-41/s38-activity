const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/details", (req,res) => {
	userController.getProfile({userId: req.body.id}).then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/getAllUser", (req,res) => {
	userController.getAllUser().then(
		resultFromController => res.send(resultFromController)
		);
});



module.exports = router;