// Setup dependencies

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// "Require" from different directories

const userRoute = require("./routes/userRoute.js");
const courseRoute = require("./routes/courseRoute.js");

// Server Setup

const app = express();


// Middlewares

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection

mongoose.connect("mongodb+srv://ajbmrls:admin123@zuitt-bootcamp.mbim4pe.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database.`));

// Server Listening
// Will be used to define port number for the application wherever environment variable is
// This syntax will allow felxibility when using the application locally or a hosted api
app.listen(process.env.Port || 4000, () => console.log(`Now connected to port ${process.env.Port || 4000}!`));

// Add task route
// Allows all the task routes created in the "userRoute.js" file to use "/user" route
app.use("/users", userRoute);
app.use("/courses", courseRoute);

